#FROM ace-server-prod:latest-amd64
FROM cp.icr.io/cp/appc/ace-server-prod@sha256:2771ba69f5871c37979576389f5a2040213b913f4d46bfbcafdabdea808f5bad
#FROM openliberty/open-liberty:kernel-java8-openj9-ubi

COPY cp4ijava.bar /home/aceuser/initial-config/bars/cp4ijava.bar
